#include <cmath>
#include <complex>
//10fps
class fractal
{
	public:
		const int width,height,framesCount,itersLimit;
		double fractalWidth,fractalHeight;
		double animationParameter;
		void frame(int frameIndex);
		void colorFunction(uint8_t& red, uint8_t& green, uint8_t& blue,
		                   int N,double X=-1,double Y=-1,
		                   std::complex<double> z = std::complex<double>(0,0));
		void fractalFirst(std::complex<double> &z0, double X, double Y);
		void fractalFunction(std::complex<double> &z, double X, double Y);
		double dx,dy;
		fractal();
};

// frameIndex >= 1
void fractal::frame(int frameIndex)
{
	animationParameter=1-0.015*frameIndex;
}

void fractal::fractalFirst(std::complex<double> &z0, double X, double Y)
{
	z0 = std::complex<double>(X,Y);
}

void fractal::fractalFunction(std::complex<double> &z, double X, double Y)
{
	z = pow(tan(z)*abs(z),-animationParameter+0.06)+pow(cos(z),animationParameter-0.06);
}

// 0 <= N < 0xFF
void fractal::colorFunction(uint8_t& red, uint8_t& green, uint8_t& blue, int N,double X,double Y, std::complex<double> z)
{
	red = 0;
	green = N*N%0xFF;
	blue = N*N*N%0xFF;
}

fractal::fractal() : width(600),
                     height(600),
                     framesCount(40),
                     itersLimit(350),
                     fractalWidth(3),
                     fractalHeight(3)
{
	dx   = +1.5; // fractalWidth / 2.
	dy   = +1.5; // fractalHeight / 2.
}

