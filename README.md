# for compile and run: #
	`./make`
	`time ./run`
	
# for clean: #
	`./make clean`
	`rm -rf ./*.bmp`

# it makes gif animation with 20 fps (must be installed ImageMagick): #
	`convert -delay 5 -loop 0 ./*.bmp ./anim.gif`