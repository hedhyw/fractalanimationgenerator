#include "./bitmapCore.hpp"
#include "parameters.hpp"
#include <fstream>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <thread>
#include <stack>
using namespace std;

#define QUIET false
const int firstFrame = 1; // >= 1
const int threadCount = 2; // >= 1

string leadingZero(int N, size_t count = 4)
{
	stringstream tmp;
	tmp << setfill('0') << setw(count) << N;
	return tmp.str();
}

void generate(fractal f, int frame, int dFr, stack<int> *frames, int *processingProgress)
{
	bitmapCore::pixel* pxls = new bitmapCore::pixel[f.width];
	while(!frames->empty())
	{
		int frame = frames->top(); frames->pop();
		f.frame(frame);
		ofstream ofs(leadingZero(frame)+".bmp", ios_base::binary | ios_base::trunc);
		bitmapCore bmp(&ofs);
		bmp.writeHeader(f.width, f.height);
		std::complex<double> z;
		for (int tY = f.height; tY--;){
			for (int tX = 0; tX < f.width; ++tX){
				double X = (tX*f.fractalWidth/f.width-f.dx),
					   Y = (tY*f.fractalHeight/f.height-f.dy);
				f.fractalFirst(z,X,Y);
				int I;
				for (I=0; I < f.itersLimit; ++I){
					f.fractalFunction(z,X,Y);
					if (abs(z)>min(f.width,f.height)) break;
				}
				uint8_t red,green,blue;
				f.colorFunction(red,green,blue,(I*255./f.itersLimit),X,Y,z);
				pxls[tX] = {blue,green,red};
			}
			bmp.writeLine(pxls);
			bmp.endLine();
		}
		ofs.close();
		#if !QUIET
		/* not (f.framesCount - frames->size()) */
		cout << "\r     " << ++(*processingProgress) << " of " << f.framesCount << " processed";
		cout.flush();
		#endif
	}
	delete [] pxls;
}

int main()
{
		
	fractal f;
	#if !QUIET
	cout << "\r     0 of " << f.framesCount << " processed";
	cout.flush();
	#endif
	
	stack<int> frames;
	for (int X = f.framesCount;X>=firstFrame;frames.push(X--));
	int processingProgress = 0;
	thread* thrs = new thread[threadCount];
	for (int X = min(threadCount,f.framesCount); X--;)
		thrs[X] = thread(generate,f,X+firstFrame,threadCount,&frames,&processingProgress);
	for (int X = min(threadCount,f.framesCount); X--;thrs[X].join());
	delete [] thrs;

	return 0;
}
