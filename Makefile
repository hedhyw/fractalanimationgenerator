OUT_FILE = run
LONG_BIT = $(shell getconf LONG_BIT)
ifeq ($(LONG_BIT),64)
	FLAGS=-m64
else
	FLAGS=-m32 -mfpmath=sse
endif
all:
	g++ main.cpp -o $(OUT_FILE) -std=gnu++11 -Ofast -march=native -pthread $(FLAGS)
	$(info execute  `time ./$(OUT_FILE)` for running)
clean:
	rm -rf *.o run
	
