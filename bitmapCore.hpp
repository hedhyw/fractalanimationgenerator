#include <fstream>
// {blue, green, red} color format
// start point in bottom left

class bitmapCore
{
	private:
		std::ofstream *ofs;
		char align[4] = {0,0,0,0};
		#pragma pack(push,1)
		struct bitmapFileHeader
		{
			uint8_t type[2] = {'B', 'M'}; // Bitmap
			uint32_t size;
			uint16_t res[2] = {0,0}; // reserve
			uint32_t offset;
		} bitmapFileHeader1;
		struct bitmapInformationHeader // CORE ver.
		{
			uint32_t header_size;
			uint16_t width;
			uint16_t height;
			uint16_t planes = 1; // Bitmap
			uint16_t bits = 24; // rgb
		} bitmapInformationHeader1;
		#pragma pack(pop)
	public:
		#pragma pack(push,1)
		struct pixel
		{
			uint8_t blue;
			uint8_t green;
			uint8_t red;
		};
		#pragma pack(pop)
		uint16_t getWidth();
		uint16_t getHeight();
		void writePixel(pixel pxl);
		void writeLine(pixel* pxls);
		void endLine();
		void writeHeader(uint16_t width, uint16_t height);
		void setOutputFileStream(std::ofstream *outputFileStream);
		bitmapCore();
		bitmapCore(std::ofstream *outputFileStream);
};

uint16_t bitmapCore::getWidth()
{
	return bitmapInformationHeader1.width;
}

uint16_t bitmapCore::getHeight()
{
	return bitmapInformationHeader1.height;
}

void bitmapCore::writePixel(pixel pxl)
{
	ofs->write(reinterpret_cast<char*>(&pxl),sizeof(pxl));
}

void bitmapCore::writeLine(pixel* pxls)
{
	ofs->write(reinterpret_cast<char*>(pxls), sizeof(pixel)*getWidth());
}

void bitmapCore::endLine()
{
	ofs->write(align,getWidth() % 4);
}

void bitmapCore::writeHeader(uint16_t imageWidth, uint16_t imageHeight)
{
	bitmapInformationHeader1.header_size = sizeof(bitmapInformationHeader1);
	bitmapInformationHeader1.width = imageWidth;
	bitmapInformationHeader1.height = imageHeight;
	
	bitmapFileHeader1.offset = sizeof(bitmapFileHeader1) + sizeof(bitmapInformationHeader1);
	bitmapFileHeader1.size = bitmapInformationHeader1.bits/8 * imageWidth * imageHeight;
	bitmapFileHeader1.size += (imageWidth%4) * imageHeight;
	bitmapFileHeader1.size += bitmapFileHeader1.offset;
	
	ofs->write(reinterpret_cast<char*>(&bitmapFileHeader1),sizeof(bitmapFileHeader1));
	ofs->write(reinterpret_cast<char*>(&bitmapInformationHeader1), sizeof(bitmapInformationHeader1));
}

bitmapCore::bitmapCore(std::ofstream *outputFileStream)
{
	setOutputFileStream(outputFileStream);
}

void bitmapCore::setOutputFileStream(std::ofstream *outputFileStream)
{
	ofs = outputFileStream;
}

/* example.cpp

#include "./bitmapCore.hpp"
#include <fstream>
#include <cmath>
using namespace std;

int main(int argc, char **argv)
{
	ofstream ofs("img.bmp",ios_base::trunc);
	bitmapCore bmp(&ofs);
	bitmapCore::pixel* pxls = new bitmapCore::pixel[200];
	bmp.writeHeader(200,200);
	for (int Y = 0; Y < bmp.getHeight(); ++Y)
	{
		for (int X = 0; X < bmp.getWidth(); ++X)
		{
			// {blue%0xFF, green%0xFF, red%0xFF}
			pxls[X] = {(uint8_t)(0xFF*(X/(double)bmp.getWidth())),
			           (uint8_t)(0xFF*(Y/(double)bmp.getHeight())),
			           0};
		}
		bmp.writeLine(pxls);
		bmp.endLine();
	}
	delete [] pxls;
	ofs.close();
	return 0;
}

*/
